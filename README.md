# README.md

Quick-and-dirty script to help prepare Arch Linux on an SD card for a Raspberry
Pi. Most useful on a Mac because it cannot create Linux partition with the
proper file system (ext2/3/4) out of the box.

## Requirement

* Vagrant (http://www.vagrantup.com/downloads.html)
* VirtualBox (https://www.virtualbox.org/wiki/Downloads)
* An SD card reader that can be connected to a VirtualBox VM. Built-in card
  readers on a Mac may not work. For example, the one on a 15" MBP Retina (2012)
  is connected on the PCI-e bus. The one on my 13" MBA (Mid-2013) crashes
  VirtualBox for some reason

## Usage

On your laptop/desktop:

1.  Clone this repo (obviously)
1.  Download the root file system from http://os.archlinuxarm.org/os/rpi/ and
    place the tarball at the root of this repo
1.  Run `vagrant up`. It will fire up a ubuntu VM
1.  "Connect" the SD card/reader to the VM. If you are on a mac, you may have
    to run `diskutil unmountdisk /dev/disk2` (or some such) to unmount it from
    the host
1.  `vagrant ssh` into the VM

On the vagrant VM:

1.  `cd /vagrant`
1.  Edit `prep-card.sh` so that `DISK` refers to the SD card, and `TARBALL` is
    the path to the root file system tarball
1.  `sudo su` to become root (important)
1.  `sudo ./prep-card.sh`

## TODO

* Parameterise the script so you don't have to edit it
