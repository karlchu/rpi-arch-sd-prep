#!/bin/bash
set -e

# http://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2

DISK=/dev/sdb
TARBALL=ArchLinuxARM-rpi-2-latest.tar.gz

BOOT=/tmp/boot
ROOT=/tmp/root

echo "### Repartition the SD card ###"
fdisk ${DISK} <<EOF
o
p
n
p
1

+100M
t
c
n
p
2


w
EOF

sleep 1

echo "### Create and mount the boot partition ###"
mkfs.vfat ${DISK}1
rm -Rf ${BOOT}
mkdir -p ${BOOT}
mount ${DISK}1 ${BOOT}

echo "### Create and mount the root partition ###"
mkfs.ext4 ${DISK}2
rm -Rf ${ROOT}
mkdir -p ${ROOT}
mount ${DISK}2 ${ROOT}

sleep 1

echo "### Extract the root filesystem ###"
time bsdtar -xvpf ${TARBALL} -C ${ROOT}
sync

echo "### Move boot files to the first partition ###"
mv ${ROOT}/boot/* ${BOOT}

sleep 1

echo "### Unmount the partitions ###"
umount ${ROOT} ${BOOT}
